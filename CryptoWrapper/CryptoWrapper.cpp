// SerializerWrapper.cpp : DLL을 위해 내보낸 함수를 정의합니다.
//

#include "pch.h"
#include "CryptoWrapper.h"

// node js용 wrapping export func.
char* Encrypt_s(char* plain)
{
	char* pCipher = nullptr;

	int ret = CCryptoLib::EncryptOpenSSL((unsigned char*)plain, (unsigned char**)&pCipher);
	
	return pCipher;
}

char* Decrypt_s(char* cipher)
{
	char* pPlain = nullptr;

	int ret = CCryptoLib::DecryptOpenSSL((unsigned char*)cipher, (unsigned char**)&pPlain);
	
	return pPlain;
}

// C# library용 wrapping export func.
WCHAR* Encrypt(WCHAR* plain)
{
	// C#은 unicode char를 사용하기 때문에 wchar을 사용.
	WCHAR* cipher = nullptr;
	unsigned char* chCipher = nullptr;
	unsigned char* chPlain = nullptr;
	int cipherLen = 0;

	// 전달 받은 unicode char를 multibyte char로 변환.
	// 변환할 문자열의 크기를 계산.
	int strSize = WideCharToMultiByte(CP_ACP, 0, plain, -1, NULL, 0, NULL, NULL);
	int arraySize = strSize + 1;

	// 계산한 크기만큼의 메모리를 할당.
	chPlain = (unsigned char*)malloc(sizeof(unsigned char) * arraySize);

	// 변환.
	WideCharToMultiByte(CP_ACP, 0, plain, -1, (char*)chPlain, strSize, 0, 0);

	// c++ library의 암호화 함수 호출
	cipherLen = CCryptoLib::EncryptOpenSSL(chPlain, &chCipher);
	
	// 암호화된 multibyte char를 C# library에 전달하기 위해서 unicode char로 변환한다.
	// 변환할 대상 문자열의 길이 계산.
	int nLen = MultiByteToWideChar(CP_ACP, 0, (char*)chCipher, strlen((char*)chCipher), NULL, NULL);
	int arrayLen = nLen + 1;
	
	// 계산한 길이 만큼 메모리를 할당.
	cipher = (WCHAR*)malloc(sizeof(WCHAR) * arrayLen);
	
	// 할당한 메모리 초기화.
	memset(cipher, 0, arrayLen);
	
	// 변환.
	MultiByteToWideChar(CP_ACP, 0, (char*)chCipher, strlen((char*)chCipher), cipher, nLen);

	// 문자열의 끝을 알려주기 위하여 널문자 삽입.
	cipher[nLen] = '\0';

	// 할당한 메모리 해제.
	free(chPlain);
	free(chCipher);

	return cipher;
}

WCHAR* Decrypt(WCHAR* cipher)
{
	WCHAR* plain = nullptr;
	unsigned char* pPlain = nullptr;
	int ret = -1;

	// 전달 받은 unicode char array를 multibyte char array로 변환.
	// 변환할 사이즈 계산.
	int strSize = WideCharToMultiByte(CP_ACP, 0, (const WCHAR*)cipher, -1, NULL, 0, NULL, NULL);

	// 계산한 크기만큼의 메모리 할당.
	unsigned char* chCipher = (unsigned char*)malloc(sizeof(unsigned char) * strSize);

	// 변환.
	WideCharToMultiByte(CP_ACP, 0, (const WCHAR*)cipher, -1, (char*)chCipher, strSize, 0, 0);

	// c++ library의 복호화 함수 호출.
	ret = CCryptoLib::DecryptOpenSSL(chCipher, &pPlain);
	
	// multibyte char array를 C# library에 전달하기 위하여 unicode char array로 변환한다.
	// 변환할 길이 계산.
	int nLen = MultiByteToWideChar(CP_ACP, 0, (char*)pPlain, strlen((char*)pPlain), NULL, NULL);

	// 계산된 만큼의 메모리 할당.
	plain = (WCHAR*)malloc(sizeof(WCHAR) * (nLen + 1));

	// 할당한 메모리 초기화.
	memset(plain, 0, nLen + 1);
	
	// 변환.
	MultiByteToWideChar(CP_ACP, 0, (char*)pPlain, strlen((char*)pPlain), plain, nLen);

	// 문자열의 끝을 알려주기 위한 널문자 삽입.
	plain[nLen] = '\0';
	
	// 사용이 끝난 메모리 할당 해제.
	free(chCipher);
	free(pPlain);

	return plain;
}
