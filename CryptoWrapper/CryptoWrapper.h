#pragma once
#include "CryptoLib.h"
#ifdef CRYPTOWRAPPER_EXPORTS
#define CRYPTWRAPPER_API __declspec(dllexport)
#else
#define CRYPTWRAPPER_API __declspec(dllimport)
#endif

extern "C"
{
	CRYPTWRAPPER_API WCHAR* Encrypt(WCHAR* plain);
	CRYPTWRAPPER_API WCHAR* Decrypt(WCHAR* cipher);
	CRYPTWRAPPER_API char* Encrypt_s(char* plain);
	CRYPTWRAPPER_API char* Decrypt_s(char* cipher);
}
#pragma once
